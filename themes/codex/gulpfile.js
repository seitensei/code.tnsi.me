var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');

gulp.task('default', ['build'], function() {

});

gulp.task('build', ['css'], function() {

});

gulp.task('css', function() {
  var plugins = [
    autoprefixer({browsers: ['last 1 vewrsion']}),
    cssnano()
  ]
  return gulp.src('./src/scss/*.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(postcss(plugins))
    .pipe(gulp.dest('./static/css'));

});
