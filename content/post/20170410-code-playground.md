+++
date = "2017-04-10T13:39:43-04:00"
draft = false
title = "Code Relaunch"

+++
Welcome back to the relaunch of [Code](http://code.tnsi.me) as a software development specific blog. My usual blog, [tnsi blog](http://blog.tnsi.me) will remain as a general blog, which may include content posted on Code.

For the time being, this blog is generated using [Hugo](http://gohugo.io/), and using the [vec](https://github.com/IvanChou/hugo-theme-vec) theme. This post will be updated once that changes.
