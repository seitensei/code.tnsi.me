# code.tnsi.me
This repository contains code for code.tnsi.me. Unless otherwise stated, post content is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), while any markup is licensed under MIT. [Hugo](https://github.com/spf13/hugo) is used to generate the site.
